local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
---@diagnostic disable-next-line: undefined-field
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup(
	{
		{
			'nvim-treesitter/nvim-treesitter',
			dependencies = { 'hiphish/rainbow-delimiters.nvim' },
			-- https://bugzilla.redhat.com/show_bug.cgi?id=2283574
			event = 'BufReadPre',
			build = ":TSUpdate",
			main = 'nvim-treesitter.configs',
			opts = {
				ensure_installed = {
					'diff',
					-- https://bugzilla.redhat.com/show_bug.cgi?id=2283574
					'lua',
					'markdown',
					'markdown_inline',
					'query',
					'vimdoc',

				},
				highlight = { enable = true },
				incremental_selection = { enable = true, keymaps = { init_selection = "gnn", node_incremental = "grn", scope_incremental = "grc", node_decremental = "grm" } },
				indent = { enable = true },
			},
			init = function()
				vim.o.foldmethod = 'expr'
				vim.o.foldexpr = 'nvim_treesitter#foldexpr()'
				vim.o.foldenable = false
			end
		},
		-- :Twilight will dim inactive (not in scope) code
		{
			'folke/twilight.nvim',
			opts = {
				dimming = {
					alpha = 0.75,
				},
				context = 5,
				expand = {
					"function",
					"if_statement",
					"method",
					"table",
				},
			},
			cmd = { 'Twilight', 'TwilightEnable', 'TwilightDisable' },
			keys = {
				{
					"<F5>",
					"<cmd>Twilight<cr>",
					desc = "Dim inactive code",
				},
			},

		},
		-- tokyonight provides our colorscheme
		'folke/tokyonight.nvim',
		{
			'VonHeikemen/lsp-zero.nvim',
			branch = 'v3.x',
			init = function()
				-- Disable automatic setup, we are doing it manually
				vim.g.lsp_zero_extend_cmp = 0
				vim.g.lsp_zero_extend_lspconfig = 0
			end,
		},
		{
			'williamboman/mason.nvim',
			opts = {},
			cmd = { 'Mason' },
		},
		-- Autocompletion
		{
			'github/copilot.vim',
			-- I can't quiet explain it, but setting up with lazy=true
			-- and event=InsertEnter, we never get Copilot connecting to lspconfig
			-- and if we do VeryLazy, the perceived startup time is worse
			lazy = false,
			cmd = { 'Copilot' },
			init = function()
				vim.g.copilot_no_tab_map = true
			end,
		},
		{
			'CopilotC-Nvim/CopilotChat.nvim',
			branch = 'canary',
			dependencies = { 'github/copilot.vim', 'nvim-lua/plenary.nvim' },
			opts = {
				window = {
					layout = 'float',
				},
			},
			keys = {
				{
					"<F1>",
					function()
						local input = vim.fn.input("Quick Chat: ")
						if input ~= "" then
							require("CopilotChat").ask(input, { selection = require("CopilotChat.select").buffer })
						end
					end,
					desc = "Write an explanation for the active selection as paragraphs of text",
					mode = { 'n' },
				},
				{
					"<F1>",
					"<Cmd>CopilotChatExplain<CR>",
					desc = "Write an explanation for the active selection as paragraphs of text",
					mode = { 'v' },
				},
			},
		},
		{
			'hrsh7th/nvim-cmp',
			event = 'InsertEnter',
			dependencies = {
				-- sources
				'hrsh7th/cmp-buffer',
				'hrsh7th/cmp-path',
				'hrsh7th/cmp-nvim-lsp',
				'hrsh7th/cmp-nvim-lua',
				'hrsh7th/cmp-nvim-lsp-signature-help',
				'CopilotC-Nvim/CopilotChat.nvim',
				'Dynge/gitmoji.nvim',
				'petertriho/cmp-git', 'nvim-lua/plenary.nvim', -- plenary is a dependency of cmp-git
				'pontusk/cmp-vimwiki-tags',
				-- snippets
				'L3MON4D3/LuaSnip',
				'rafamadriz/friendly-snippets',
				'saadparwaiz1/cmp_luasnip',
			},
			config = function()
				local lsp_zero = require('lsp-zero')
				lsp_zero.extend_cmp()

				local cmp = require('cmp')
				local cmp_action = lsp_zero.cmp_action()

				require("luasnip.loaders.from_lua").lazy_load({ paths = "./snippets" })
				require('luasnip.loaders.from_vscode').lazy_load()

				cmp.setup({
					formatting = lsp_zero.cmp_format({ details = true }),
					mapping = cmp.mapping.preset.insert({
						['<C-Space>'] = cmp.mapping.complete(),
						['<C-u>'] = cmp.mapping.scroll_docs(-4),
						['<C-d>'] = cmp.mapping.scroll_docs(4),
						['<C-f>'] = cmp_action.luasnip_jump_forward(),
						['<C-b>'] = cmp_action.luasnip_jump_backward(),
					}),
					snippet = {
						expand = function(args)
							require('luasnip').lsp_expand(args.body)
						end,
					},
					sources = {
						{ name = 'path' },
						{ name = 'nvim_lsp' },
						{ name = 'nvim_lua' },
						{ name = 'buffer',                 keyword_length = 3 },
						{ name = 'luasnip',                keyword_length = 2 },
						{ name = 'nvim_lsp_signature_help' },
						{ name = 'git' },
						{ name = 'gitmoji',                priority = 5 },
						{ name = 'vimwiki-tags' },
					},
				})

				require('CopilotChat.integrations.cmp').setup()
				require('cmp_git').setup()
			end
		},
		-- LSP
		{
			'neovim/nvim-lspconfig',
			cmd = { 'LspInfo', 'LspInstall', 'LspStart' },
			event = { 'BufReadPre', 'BufNewFile' },
			dependencies = {
				'hrsh7th/cmp-nvim-lsp',
				'williamboman/mason-lspconfig.nvim',
			},
			config = function()
				local lsp_zero = require('lsp-zero')
				lsp_zero.extend_lspconfig()

				lsp_zero.on_attach(function(_, bufnr)
					-- :help lsp-zero-keybindings
					lsp_zero.default_keymaps({ buffer = bufnr })
				end)

				require('mason-lspconfig').setup({
					handlers = {
						function(server_name)
							require('lspconfig')[server_name].setup({})
						end,

						lua_ls = function()
							local lua_opts = lsp_zero.nvim_lua_ls()
							require('lspconfig').lua_ls.setup(lua_opts)
						end,

						astro = function()
							require('lspconfig').astro.setup({
								settings = {
									astro = {
										typescript = {
											inlayHints = {
												parameterNames = {
													enabled = "all",
												}
											}
										}
									}
								}
							})
						end
					}
				})
			end
		},
		-- Language specific
		{
			'ray-x/go.nvim',
			dependencies = {
				'ray-x/guihua.lua'
			},
			ft = { 'go', 'gomod' },
			build = ':lua require("go.install").update_all_sync()',
			opts = {},
		},
		{
			'iamcco/markdown-preview.nvim',
			ft = 'markdown',
			cmd = { 'MarkdownPreviewToggle', 'MarkdownPreview' },
			build = function()
				vim.fn["mkdp#util#install"]()
			end

		},
		-- Utilities
		{
			'folke/which-key.nvim',
			dependencies = {
				'echasnovski/mini.icons',
				'nvim-tree/nvim-web-devicons',
			},
			event = 'VeryLazy',
			init = function()
				vim.o.timeout = true
				vim.o.timeoutlen = 300
			end,
			config = function()
				local wk = require("which-key")
				wk.add({
					{ "<leader>c", group = "LSP" },
					{ "<leader>f", group = "Telescope" },
					{ "<leader>r", group = "Refactor" },
					{ "<leader>w", group = "Vimwiki" },
					{ "<leader>x", group = "Trouble" },
				})
			end
		},
		{
			'ray-x/lsp_signature.nvim',
			event = 'InsertEnter',
			opts = {},
		},
		{
			'vimwiki/vimwiki',
			branch = 'dev',
			cmd = { 'VimwikiIndex', 'VimwikiDiaryIndex', 'VimwikiMakeDiaryNote' },
			keys = {
				{
					"<leader>ww",
					"<cmd>VimwikiIndex<cr>",
					desc = "Open index file of the current wiki.",
				},
				{
					"<leader>wi",
					"<cmd>VimwikiDiaryIndex<cr>",
					desc = "Open diary index file of the current wiki.",
				},
				{
					"<leader>w<leader>w",
					"<cmd>VimwikiMakeDiaryNote<cr>",
					desc = "Open diary wiki-file for today of the current wiki.",
				},
			},
			init = function()
				vim.g.vimwiki_list = {
					{
						path = "~/.vimwiki",
						path_html = "~/.vimwiki-html",
						auto_export = 1,
						auto_toc = 1,
						diary_frequency = "weekly",
						cycle_bullets = 1,
						generated_links_caption = 1,
						auto_tags = 1,
						auto_diary_index = 1,
						auto_generate_links = 1,
						auto_generate_tags = 1,
						hl_headers = 1,
						hl_cb_checked = 1,
						dir_link = "index",
						auto_chdir = 1,
						listing_hl = 1,
						auto_header = 1,
					},
				}
				vim.g.vimwiki_hl_headers = 1
				vim.g.vimwiki_hl_cb_checked = 1
				vim.g.vimwiki_dir_link = "index"
				vim.g.vimwiki_auto_chdir = 1
				vim.g.vimwiki_listing_hl = 1
				vim.g.vimwiki_auto_header = 1
			end,
		},
		{
			'rmagatti/auto-session',
			event = 'VeryLazy',
			init = function()
				vim.o.sessionoptions =
				"blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal,localoptions"
			end,
			opts = {
				auto_session_enable_last_session = true,
				auto_save_enabled = true,
				auto_restore_enabled = true,
				log_level = "info",
				auto_session_last_session_dir = vim.fn.stdpath("data") .. "/sessions/",
				auto_session_root_dir = vim.fn.stdpath("data") .. "/sessions/",
				auto_session_enabled = true,
			},
		},
		{ 'tpope/vim-sensible',  event = 'VeryLazy' },
		{ 'scrooloose/nerdtree', cmd = { 'NERDTree' } },
		{ 'tpope/vim-fugitive',  cmd = { 'Git' } },
		{
			'nvim-telescope/telescope.nvim',
			cmd = 'Telescope',
			tag = '0.1.6',
			dependencies = { 'nvim-lua/plenary.nvim' },
			opts = {},
		},
		-- Linters
		{
			'nvimtools/none-ls.nvim',
			main = 'null-ls',
			opts = {},
		},
		{
			'zeioth/none-ls-autoload.nvim',
			event = 'BufEnter',
			dependencies = { 'williamboman/mason.nvim', 'nvimtools/none-ls.nvim' },
			opts = {},
		},
		{
			'folke/trouble.nvim',
			cmd = 'Trouble',
			opts = {},
			keys = {
				{
					"<leader>xx",
					"<cmd>Trouble diagnostics toggle<cr>",
					desc = "Diagnostics (Trouble)",
				},
				{
					"<leader>xX",
					"<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
					desc = "Buffer Diagnostics (Trouble)",
				},
				{
					"<leader>cs",
					"<cmd>Trouble symbols toggle focus=false<cr>",
					desc = "Symbols (Trouble)",
				},
				{
					"<leader>cl",
					"<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
					desc = "LSP Definitions / references / ... (Trouble)",
				},
				{
					"<leader>xL",
					"<cmd>Trouble loclist toggle<cr>",
					desc = "Location List (Trouble)",
				},
				{
					"<leader>xQ",
					"<cmd>Trouble qflist toggle<cr>",
					desc = "Quickfix List (Trouble)",
				},
			},

		},

		-- DAP
		{
			'theHamsta/nvim-dap-virtual-text',
			opts = {},
			dependencies = { 'mfussenegger/nvim-dap' }
		},
		{
			'rcarriga/nvim-dap-ui',
			dependencies = { 'mfussenegger/nvim-dap', 'nvim-neotest/nvim-nio' },
			opts = {},
		},

	},
	{
		defaults = {
			lazy = true
		},
	}

)
