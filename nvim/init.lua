vim.wo.number = true
vim.o.breakindent = true
vim.opt.undofile = true
vim.o.updatetime = 250
vim.o.termguicolors = true
vim.g.mapleader = ','
vim.g.maplocalleader = ','
vim.o.completeopt = 'menuone,noselect'

require("plugins")
require("mappings")

vim.cmd("colorscheme tokyonight")
vim.g.loaded_perl_provider = 0
vim.g.loaded_ruby_provider = 0

vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

-- vim.opt.completeopt = { "menu", "menuone", "noselect" }
