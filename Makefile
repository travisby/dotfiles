.PHONY: all clean

all: $${HOME}/.gitconfig $${HOME}/.config/nvim/init.lua $${HOME}/.config/nvim/lua/mappings.lua $${HOME}/.config/nvim/snippets/go.lua $${HOME}/.config/nvim/lua/plugins.lua $${HOME}/.gnupg/gpg.conf $${HOME}/.gnupg/scdaemon.conf $${HOME}/.bashrc


$${HOME}/.gitconfig: gitconfig
	cp gitconfig $${HOME}/.gitconfig

$${HOME}/.config/nvim:
	mkdir -p $${HOME}/.config/nvim

$${HOME}/.config/nvim/lua:
	mkdir -p $${HOME}/.config/nvim/lua

$${HOME}/.config/nvim/init.lua: nvim/init.lua $${HOME}/.config/nvim
	cp nvim/init.lua $${HOME}/.config/nvim/init.lua

$${HOME}/.config/nvim/lua/mappings.lua: nvim/lua/mappings.lua $${HOME}/.config/nvim/lua
	cp nvim/lua/mappings.lua $${HOME}/.config/nvim/lua/mappings.lua

$${HOME}/.config/nvim/lua/plugins.lua: nvim/lua/plugins.lua $${HOME}/.config/nvim/lua
	cp nvim/lua/plugins.lua $${HOME}/.config/nvim/lua/plugins.lua

$${HOME}/.config/nvim/snippets:
	mkdir -p $${HOME}/.config/nvim/snippets

$${HOME}/.config/nvim/snippets/go.lua: $${HOME}/.config/nvim/snippets nvim/snippets/go.lua
	cp nvim/snippets/go.lua $${HOME}/.config/nvim/snippets/go.lua

$${HOME}/.gnupg:
	mkdir -p $${HOME}/.gnupg

$${HOME}/.gnupg/gpg.conf: .gnupg/gpg.conf
	cp .gnupg/gpg.conf $${HOME}/.gnupg/

$${HOME}/.gnupg/scdaemon.conf: .gnupg/scdaemon.conf
	cp .gnupg/scdaemon.conf $${HOME}/.gnupg/

$${HOME}/.bashrc:
	cp .bashrc $${HOME}/
