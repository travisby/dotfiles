# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

PATH="$PATH:$HOME/.bin:$HOME/.npm/bin/"
alias vim=nvim

# ignore gpg1 for smartcard reasons
if which gpg2 >/dev/null; then
	alias gpg='gpg2'
fi

# support SSH agent from smartcard
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi

# take the exclusive lock on pcsc at login so nothing else does
gpg-connect-agent updatestartuptty /bye >/dev/null
gpg2 --card-status >/dev/null

alias k=kubectl
